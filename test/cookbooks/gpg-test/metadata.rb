name 'gpg-test'
maintainer 'Make.org'
maintainer_email 'sre@make.org'
license 'Apache-2.0'
description 'Cookbook to test gpg'
long_description 'Cookbook to test gpg'
source_url 'https://gitlab.com/chef-platform/gpg-platform'
issues_url 'https://gitlab.com/chef-platform/gpg-platform/issues'
version '1.0.0'

supports 'centos', '>= 7.1'

chef_version '>= 12.19'

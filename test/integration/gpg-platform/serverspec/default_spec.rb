#
# Copyright (c) 2016-2017 Sam4Mobile, 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'gpg' do
  describe command('gpg --keyring /root/.gnupg/pubring.gpg '\
                   '--list-keys') do
    its(:stdout) { should contain('root@localhost') }
  end

  describe command('gpg --keyring /home/test-create/.gnupg/pubring.gpg '\
                   '--list-keys') do
    its(:stdout) { should contain('test@localhost') }
  end

  describe command('ls -l /home/test-delete/.gnupg/pubring.gpg') do
    its(:stderr) { should match(/No such file or directory/) }
  end
end

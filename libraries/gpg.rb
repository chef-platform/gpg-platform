###
# Copyright (c) 2016-2017 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module GpgPlatform
  # Gpg Helper module
  module Gpg
    def home
      ::Dir.home(new_resource.user)
    end

    def import_key
      shell_out!("echo '#{new_resource.key}' | gpg --import",
                 user: new_resource.user,
                 env:  { 'HOME' => home, 'USER' => new_resource.user },
                 cwd: home)
    end

    def key_fingerprint
      gpg_output =
        shell_out!("echo '#{new_resource.key}' | "\
                   'gpg --with-colons --with-fingerprint')
      return if gpg_output.stdout.empty?
      gpg_output.stdout.lines.map(&:chomp)
                .keep_if { |e| e.start_with?('fpr') }
                .first.split(':')[9]
    end

    # rubocop:disable Metrics/AbcSize
    def keys_list
      list_output =
        shell_out!('gpg --with-colons --with-fingerprint --list-keys',
                   user: new_resource.user,
                   env:  { 'HOME' => home, 'USER' => new_resource.user },
                   cwd: home)
      list_output.stdout.lines.map(&:chomp)
                 .keep_if { |e| e.start_with?('fpr') }.map do |key|
        key.split(':')[9]
      end
    end
    # rubocop:enable Metrics/AbcSize

    def delete_key
      shell_out!("gpg --delete-key #{key_fingerprint}",
                 user: new_resource.user,
                 env:  { 'HOME' => home, 'USER' => user },
                 cwd: home)
      Chef::Log.info("Deleted key for #{user}, fingerprint #{key_fingerprint}")
    end

    def in_keyring?
      keys_list.include?(key_fingerprint)
    end
  end
end

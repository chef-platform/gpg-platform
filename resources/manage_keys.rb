#
# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
default_action :create

property :user, String
property :key, String

action_class do
  include GpgPlatform::Gpg
end

action :create do
  directory "#{home}/.gnupg" do
    user new_resource.user
  end.run_action(:create)

  action_import
end

action :import do
  ruby_block 'import_key' do
    block do
      import_key
    end
    not_if { in_keyring? }
  end
end

action :delete do
  ruby_block 'delete_key' do
    block do
      delete_key
    end
    only_if { in_keyring? }
  end

  directory "#{home}/.gnupg" do
    recursive true
    action :delete
  end
end
